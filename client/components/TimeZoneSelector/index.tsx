import React from "react";

interface IProps {
    onChanged(offset: number): void;
}

interface IState {
    timezones: Array<string>;
    selectedTimezoneId: string;
    selectedOffset: number;
}

export class TimeZoneSelector extends React.Component<IProps, IState> {
    private readonly refSelect = React.createRef<HTMLSelectElement>();

    render() {
        const selId = this.state && this.state.selectedTimezoneId ? this.state.selectedTimezoneId : 'UTC';
        const options = this.state && this.state.timezones ?
            this.state.timezones.map((val, index) => {
                if (selId === val) {
                    return <option key={index} value={val} selected>{val}</option>;
                } else {
                    return <option key={index} value={val}>{val}</option>;
                }
            }) :
            [];

        return <div>
            <select ref={this.refSelect}>
                <option></option>
                {options}
            </select>
            <button onClick={this.onSetTimeZone.bind(this)}>Set</button>
            <div>Name: {this.state && this.state.selectedTimezoneId ? this.state.selectedTimezoneId : 'n/a'}</div>
            <div>
                Offset from UTC:&nbsp;
                {this.state && this.state.selectedOffset ? this.state.selectedOffset : 'n/a'} seconds /&nbsp;
                {this.state && this.state.selectedOffset ? (this.state.selectedOffset / 3600) : 'n/a'} hours
            </div>
        </div>;
    }

    componentWillMount() {
        this.loadTimeZones();
        this.loadConfig();
    }

    private onSetTimeZone() {
        const id = this.refSelect.current ? this.refSelect.current.value : null;

        if (id) {
            // alert('setting timezone ' + id);
            const self = this;
            window.fetch('/api/Config', { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ timezone: { id: id } }) }).then(
                (response) => {
                    response.json().then(
                        (config) => self.onRetrieveConfig(config)
                    )
                },
                (error) => {
                    console.error('Could not retrieve config.', error);
                }
            );
        }
    }

    private loadConfig() {
        const self = this;

        window.fetch('/api/Config').then(
            (response) => {
                response.json().then(
                    (config) => self.onRetrieveConfig(config)
                )
            },
            (error) => {
                console.error('Could not retrieve config.', error);
            }
        );
    }

    private onRetrieveConfig(config: any) {
        this.setState({ selectedTimezoneId: config.timezone.id, selectedOffset: config.timezone.offset });

        if (this.props.onChanged) {
            this.props.onChanged(config.timezone.offset);
        }
    }

    private loadTimeZones() {
        const self = this;

        window.fetch('/api/TimeZones').then(
            (response) => {
                response.json().then(
                    (data) => {
                        self.setState({ timezones: data as Array<string> });
                    }
                )
            },
            (error) => {
                console.error('Could not retrieve timezones.', error);
            }
        )
    }
}