import './index.css';
import React, { CSSProperties } from "react";

const angleOffset: number = Math.PI / 2;

const radius: number = 150
const indicatorSize: number = 10;

interface IProps {
    offsetSeconds: number;
}

interface IState {
    leds: Array<string>;
}

export class Clock extends React.Component<IProps, IState> {
    private intervalHandle: number = 0;

    state = { leds: new Array<string>(60) };

    // /**
    //  *
    //  */
    constructor(props: IProps) {
        super(props);

    }

    render() {
        const leds = [];
        const ticks = [];

        for (let i = 0; i < 60; i++) {
            leds[i] = this.indicator(i);

            if (i % 5 === 0) {
                ticks[i] = this.tick(i);
            }
        }

        return <div style={{ width: '300px', height: '300px', position: 'relative' }}>
            {leds}
            {ticks}
        </div>;
    }

    componentDidMount() {
        this.intervalHandle = setInterval(this.setClock.bind(this), 1000);
    }

    componentWillUnmount() {
        if (this.intervalHandle) {
            clearInterval(this.intervalHandle);
            this.intervalHandle = 0;
        }
    }

    private setClock() {
        const dt = new Date(new Date().valueOf() + (this.props.offsetSeconds * 1000));

        const h = (dt.getUTCHours() % 12) * 5;
        const m = dt.getUTCMinutes();
        const s = dt.getUTCSeconds();
        const leds = new Array<string>(60);

        for (let i = 0; i < 60; i++) {
            leds[i] = '#' + (i !== h ? '00' : 'ff') + (i !== m ? '00' : 'ff') + (i !== s ? '00' : 'ff');
        }

        this.setState({ leds: leds });
    }

    private tick(index: number) {
        const size = 18;
        const pos = this.getPos(index, radius - indicatorSize - size);
        const css: CSSProperties = {
            left: (radius + pos.x) + 'px',
            top: (radius + pos.y) + 'px',
        };

        if (index % 5 === 0) {
            return <div style={css} className='tick' key={'tick-' + index}>{index}</div>;
        }
        else {
            return <span />;
        }
    }

    private indicator(index: number) {
        const pos = this.getPos(index);
        const color = this.state.leds[index] || '#ffffff';
        const css: CSSProperties = {
            background: color,
            left: (radius + pos.x) + 'px',
            top: (radius + pos.y) + 'px',
        };

        return <div style={css} className='led' key={index}></div>;
    }

    private getPos(indicator: number, distance: number = (radius - (indicatorSize / 2))) {
        const angle = (((Math.PI * 2) / 60) * indicator) - angleOffset;

        return {
            x: Math.cos(angle) * distance,
            y: Math.sin(angle) * distance,
        };
    }
}