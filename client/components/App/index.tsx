import './index.css';
import React from "react";
import { Clock } from "../Clock";
import { TimeZoneSelector } from '../TimeZoneSelector';

interface IProps {
}

interface IState {
    offset: number;
}

export class App extends React.Component<IProps, IState> {


    render() {
        return <>
            <h1>Pi Clock with LEDs</h1>
            <div className='main'>
                <Clock offsetSeconds={(this.state && this.state.offset) || 0} />
                <TimeZoneSelector onChanged={this.onTimeZoneChanged.bind(this)} />
                {/* <div style={{ width: '200px', height: '200px', background: 'red' }}></div> */}
            </div>
        </>;
    }

    private onTimeZoneChanged(offset: number) {
        this.setState({ offset: offset });
        // alert('new offset: ' + offset);
    }
}