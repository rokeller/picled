package processing

import (
	"fmt"
	"picled/app/state"
	"time"
)

func RunClock() {
	ticker := time.NewTicker(1 * time.Second)

	go func() {
		for t := range ticker.C {
			location := state.GetTimeLocation()
			// fmt.Printf("Sync clock: %v || %v\n", t, t.In(location))

			targetTime := t.In(location)
			hour := targetTime.Hour() % 12
			minute := targetTime.Minute()
			second := targetTime.Second()
			name, offset := targetTime.Zone()

			fmt.Printf("Show  Hour: %2d  Minute: %2d  Second: %2d    Offset: %5d  Zone: %s\n", hour, minute, second, offset, name)
		}
	}()
}
