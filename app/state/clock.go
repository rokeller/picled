package state

import "time"

type clock struct {
	location *time.Location
}

var (
	currentClock = clock{
		location: time.Local,
	}
)

func GetTimeLocation() *time.Location {
	return currentClock.location
}

func SetTimeLocation(timeZoneName string) {
	currentClock.location, _ = time.LoadLocation(timeZoneName)
}

func init() {
	// currentClock.location, _ = time.LoadLocation("Europe/Moscow")
	// SetTimeLocation("America/Los_Angeles")
}
