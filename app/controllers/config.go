package controllers

import (
	"net/http"
	"picled/app/state"
	"time"

	"github.com/revel/revel"
)

type Config struct {
	*revel.Controller
}

type clockConfig struct {
	Timezone timezoneConfig `json:"timezone"`
}

type timezoneConfig struct {
	Id     string `json:"id"`
	Offset int    `json:"offset"`
}

func (c Config) Get() revel.Result {
	location := state.GetTimeLocation()
	targetTime := time.Now().In(location)
	zoneId, zoneOffset := targetTime.Zone()

	return c.RenderJSON(clockConfig{
		Timezone: timezoneConfig{
			Id:     zoneId,
			Offset: zoneOffset,
		},
	})
}

func (c Config) Put() revel.Result {
	var newConfig clockConfig

	c.Params.BindJSON(&newConfig)
	// TODO: Input validation
	state.SetTimeLocation(newConfig.Timezone.Id)

	c.Response.Status = http.StatusAccepted
	return c.Get()
}

func (c Config) Patch() revel.Result {
	return c.RenderJSON(nil)
}
