package controllers

import (
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/revel/revel"
)

var zoneDirs = []string{
	// Update path according to your OS
	"/usr/share/zoneinfo/",
	"/usr/share/lib/zoneinfo/",
	"/usr/lib/locale/TZ/",
}
var timeZones []string

type TimeZones struct {
	*revel.Controller
}

func (c TimeZones) List() revel.Result {
	return c.RenderJSON(timeZones)
}

func (c TimeZones) Get(name string) revel.Result {
	l, err := time.LoadLocation(name)

	if nil != err {
		c.Response.Status = http.StatusNotFound
		c.Response.ContentType = "application/json"

		result := make(map[string]interface{})
		result["error"] = "not_found"

		return c.RenderJSON(result)
	}

	return c.RenderJSON(l.String())
}

func init() {
	for _, zoneDir := range zoneDirs {
		readTZFile(zoneDir, "")
	}
}

func readTZFile(zoneDir, path string) {
	files, _ := ioutil.ReadDir(zoneDir + path)
	for _, f := range files {
		if f.Name() != strings.ToUpper(f.Name()[:1])+f.Name()[1:] {
			// Names start with an upper-case letter.
			continue
		}
		if f.IsDir() {
			readTZFile(zoneDir, path+"/"+f.Name())
		} else {
			name := (path + "/" + f.Name())[1:]
			timeZones = append(timeZones, name)
			// fmt.Println((path + "/" + f.Name())[1:])
		}
	}
}
